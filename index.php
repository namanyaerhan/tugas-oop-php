<?php 

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $binatang = new Animal('Shaun');
    echo 'Name : '.$binatang->name.'<br>';
    echo 'Legs : '.$binatang->legs.'<br>';
    echo 'Cold Blooded : '.$binatang->cold_blooded.'<br>';
    echo '<br>';

    $sungokong = new Ape('Kera Sakti');
    echo 'Name : '.$sungokong->name.'<br>';
    echo 'Legs : '.$sungokong->legs.'<br>';
    echo 'Cold Blooded : '.$sungokong->cold_blooded.'<br>';
    $sungokong->yell();

    echo '<br>';
    echo '<br>';
    $kodok = new Frog('Buduk');
    echo 'Name : '.$kodok->name.'<br>';
    echo 'Legs : '.$kodok->legs.'<br>';
    echo 'Cold Blooded : '.$kodok->cold_blooded.'<br>';
    $kodok->jump();

?>